using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject target;
    private Vector3 offset;

    [SerializeField]
    private float lerpRate;

    public bool gameOver;

    // Start is called before the first frame update
    private void Start()
    {
        offset = target.transform.position - transform.position;
        gameOver = false;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!gameOver)
        {
            Follow();
        }
    }

    private void Follow()
    {
        Vector3 pos = transform.position;
        Vector3 targetPos = target.transform.position - offset;
        pos = Vector3.Lerp(pos, targetPos, lerpRate * Time.deltaTime);
        transform.position = pos;
    }
}