using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Text;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public GameObject zigzagPanel;
    public GameObject gameOverPanel;
    public TextMeshProUGUI score;
    public TextMeshProUGUI highScore1;
    public TextMeshProUGUI highScore2;
    public GameObject tapText;
    public GameObject countingScore;

    // Start is called before the first frame update
    private void Start()
    {
        highScore1.text = "High Score: " + PlayerPrefs.GetInt("highScore").ToString();
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void GameStart()
    {
        tapText.SetActive(false);
        countingScore.SetActive(true);
        zigzagPanel.GetComponent<Animator>().Play("PanelUp");
        countingScore.GetComponent<Animator>().Play("Appear");
    }

    public void GameOver()
    {
        score.text = PlayerPrefs.GetInt("score").ToString();
        highScore2.text = PlayerPrefs.GetInt("highScore").ToString();
        countingScore.SetActive(false);
        gameOverPanel.SetActive(true);
    }

    public void Reset()
    {
        SceneManager.LoadScene(0);
    }
}